
/*
 * lz77.c
 *
 *  Created on: 11 Nov 2021
 *      Author: robsk
 */
#include "lz77.h"

#include <stdlib.h>
#include <string.h>

#include "circular_buffer.h"

#define WORD_LEGTH	30

struct lz77{
	cbuf_handle_t dicionary;
	cbuf_handle_t input;
};

inline lz77_handle_t lz77_create(void)
{
	lz77_handle_t lz77_han 	= malloc(sizeof(struct lz77));

	lz77_han->dicionary 	= c_buffer_create(1024);	//like `matched_position` capacity, 2^12 - 1
	lz77_han->input			=c_buffer_create(400);

	return lz77_han;
}

inline void lz77_destroy(lz77_handle_t lz77_han)
{
	if(lz77_han != NULL)
	{
		c_buffer_destroy(lz77_han->dicionary);
		c_buffer_destroy(lz77_han->input);
		free(lz77_han);
	}
}

struct commpressed_data out_data;
/**
 *  @brief compress data from data_in into data_out
 *
 *  @return output array size
 */
size_t lz77_compress( lz77_handle_t lz77_han, uint8_t *data_in, size_t element_count, uint8_t *data_out)
{
	size_t elements_analyzed = 0;
	size_t count_element;
	size_t output_count = 0;	// count elements in output buffer
	uint32_t i,j;
	uint8_t value_dic, value_in;

	
	for(i=0; i<element_count; i++)
	{
		c_buffer_add(lz77_han->input, data_in[i]);
	}

	element_count = c_buffer_count(lz77_han->input);
	
	while (element_count > elements_analyzed)
	{
		count_element = c_buffer_count(lz77_han->dicionary);
		memset(&out_data, 0, sizeof(out_data));

		for(i = 0; i<count_element; i++)
		{
			for(j=0; j<(WORD_LEGTH + 1) && i >= j && j < (element_count - elements_analyzed); j++)
			{
				value_dic = c_buffer_get_back(lz77_han->dicionary, i - j);
				value_in = c_buffer_get(lz77_han->input, j);

				if(value_dic != value_in)	break;
			}

			//Get the longest or last matched is eaqual with previous one 
			if(j >= out_data.matched_legth && j > 0)
			{
				out_data.matched_legth 		= j;
				out_data.matched_position 	= i + 1;
			}

			if(out_data.matched_legth == WORD_LEGTH) break;
		}

		if(out_data.matched_legth + 1 >= (element_count - elements_analyzed)) break;

		out_data.sentinel 	= c_buffer_get(lz77_han->input, out_data.matched_legth);

		memcpy(data_out, &out_data, sizeof(out_data));
		data_out 		+= sizeof(out_data);
		output_count 	+= sizeof(out_data);
		
		for(i=0; i<out_data.matched_legth + 1; i++)
		{
			c_buffer_add(lz77_han->dicionary, c_buffer_get(lz77_han->input, 0));
			c_buffer_pop(lz77_han->input);
			elements_analyzed++;
		}
	}
	
	return output_count;
}

/*
TODO:
	- write test decompressing value and compare it to original, should be the same
	- write test for full dictionary
	- write test for full input
*/
