#include <gtest/gtest.h>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>

extern "C" {
  #include "lz77.h"
}

uint8_t raw[] = "LOREM LOREM LOREM ";
uint8_t raw_2[] = "LOREM LOREM LOREM LOREM LOREM LOREM";
uint8_t *data_from_file;


struct commpressed_data excepted_out[] = 
{
  { 0, 0, 'L'},
  { 0, 0, 'O'},
  { 0, 0, 'R'},
  { 0, 0, 'E'},
  { 0, 0, 'M'},
  { 0, 0, ' '},
  { 6, 6, 'L'},
};

struct commpressed_data excepted_out_parts[] = 
{
  { 0,  0,  'L'},
  { 0,  0,  'O'},
  { 0,  0,  'R'},
  { 0,  0,  'E'},
  { 0,  0,  'M'},
  { 0,  0,  ' '},
  { 6,  6,  'L'},
  { 12, 12, 'O'}
};

static void print_outputs(struct commpressed_data* out, int count_elements)
{
  std::cerr<<"Elements: " <<std::endl;

  for(int i=0; i<count_elements; i++)
  {
    std::cerr << "( " << out[i].matched_legth    << ", " 
                      << out[i].matched_position << ", " 
                      << (int)out[i].sentinel         << " )" 
                      << std::endl;
  }
}

static int read_from_file(std::string path)
{
    std::fstream fs;
    int length_file;
    fs.open(path, std::fstream::in);

    if(fs)
    {
        fs.seekg(0, fs.end);
        length_file = fs.tellg();
        fs.seekg(0, fs.beg);

        data_from_file = (uint8_t *)malloc(length_file);

        fs.read((char *) data_from_file,length_file);
        fs.close();

        return length_file;
    }
    else
    {
        return 0;
    }
    
}

TEST(LZ77, HelloWorld)
{
  EXPECT_EQ(2+2, 4);
}

TEST(LZ77, CompresLorem)
{
  lz77_handle_t lz77_han;
  uint8_t out[sizeof(raw) * 3];

  size_t count_compressed;

  lz77_han = lz77_create();

  count_compressed = lz77_compress(lz77_han, raw, sizeof(raw), out);

  print_outputs((struct commpressed_data *) out, count_compressed/3);

  EXPECT_EQ(memcmp(out, excepted_out, sizeof(excepted_out)), 0);
  //EXPECT_EQ(count_compressed, sizeof(excepted_out));
}

TEST(LZ77, CompresLoremCount)
{
  lz77_handle_t lz77_han;
  uint8_t out[sizeof(raw) * 3];

  size_t count_compressed;

  lz77_han = lz77_create();

  count_compressed = lz77_compress(lz77_han, raw, sizeof(raw), out);

  EXPECT_EQ(count_compressed, sizeof(excepted_out));
}

TEST(LZ77, CompresLoremTwoTimesInOnePart)
{
  lz77_handle_t lz77_han;
  uint8_t out[1000];

  size_t count_compressed;

  lz77_han = lz77_create();

  count_compressed =  lz77_compress(lz77_han, raw_2, sizeof(raw_2), &out[0]);

  print_outputs((struct commpressed_data *) out, count_compressed/3);

  EXPECT_EQ(memcmp(out, excepted_out_parts, sizeof(excepted_out) + 3 ), 0);
}


TEST(LZ77, Compress4KbFile)
{
    uint8_t output[1500];
    lz77_handle_t lz77_han;
    int i;
    int PART_COMPRESS        = 50;
    int PART_COMPRESS_bigger = 100;
    int text_length = read_from_file("../../../tests/res/tes1t.txt");
    size_t count_compressed = 0;
    size_t count_bigger = 0;

    lz77_han = lz77_create();
    
    if(data_from_file)
    {
        //compress in smaller part
        for(i=0; i<text_length; i += PART_COMPRESS)
        {
            count_compressed +=  lz77_compress(lz77_han, &data_from_file[i], PART_COMPRESS, &output[i]);
        }
        count_compressed +=  lz77_compress(lz77_han, &data_from_file[i], text_length - i, &output[i]);
        lz77_destroy(lz77_han);

        //Compress again in bigger part
        lz77_han = lz77_create();
        for(i=0; i<text_length; i += PART_COMPRESS_bigger)
        {
            count_bigger +=  lz77_compress(lz77_han, &data_from_file[i], PART_COMPRESS_bigger, &output[i]);
        }
        count_bigger +=  lz77_compress(lz77_han, &data_from_file[i], text_length - i, &output[i]);
        lz77_destroy(lz77_han);

        free(data_from_file);
        
        // Should be exacly equal
    }

    EXPECT_EQ(count_compressed, count_bigger);
}

TEST(LZ77, CompresLoremTwoTimes)
{
  lz77_handle_t lz77_han;
  uint8_t out[1000];

  size_t count_compressed;

  lz77_han = lz77_create();

  count_compressed =  lz77_compress(lz77_han, raw, sizeof(raw) - 1, &out[0]);

  count_compressed += lz77_compress(lz77_han, raw, sizeof(raw) - 1, &out[count_compressed]);

  print_outputs((struct commpressed_data *) out, count_compressed/3);

  EXPECT_EQ(memcmp(out, excepted_out_parts, sizeof(excepted_out) +3), 0);
}
