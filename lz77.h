/*
 * lz77.h
 *
 *  Created on: 11 Nov 2021
 *      Author: robsk
 */

#ifndef LZ77_INC_LZ77_H_
#define LZ77_INC_LZ77_H_

#include <stdint.h>
#include <stddef.h>

typedef struct lz77 *lz77_handle_t;

struct commpressed_data{
	uint16_t	matched_legth 	:5;
	uint16_t	matched_position:11;
	uint8_t 	sentinel;			//sign which descibe first orignal sign

} __attribute__((packed));

lz77_handle_t 	lz77_create(void);
void 			lz77_destroy(lz77_handle_t);

size_t 			lz77_compress(lz77_handle_t, uint8_t *data_in, size_t element_count, uint8_t *data_out);

#endif /* LZ77_INC_LZ77_H_ */
